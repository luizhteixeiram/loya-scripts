import shlex, subprocess, os, sys

def write(file_path, file_content):
    print "Writing %s" % (file_path)
    file = os.open(file_path, os.O_WRONLY|os.O_CREAT)
    os.write(file, file_content)
    os.close(file)

def create_file(file_path, file_content):
    file_path_split = os.path.split(file_path)
    directory = file_path_split[0]
    try:
        write(file_path, file_content)
    except OSError:
        os.makedirs(directory)
        write(file_path, file_content)

def process(shell_command):
    args = shlex.split(shell_command)
    process = subprocess.Popen(args)
    process.wait()

def update_and_upgrade():
    shell_command = "apt-get update && apt-get upgrade"
    process(shell_command)

def install(app):
    shell_command = "apt-get install -y %s" % (app)
    process(shell_command)

def remove(app):
    shell_command = "apt-get purge -y %s" % (app)
    process(shell_command)

def change_owner(owner, file_path):
    shell_command = "chown %s %s" % (owner, file_path)
    process(shell_command)

def gen_no_mouse_accel():
    no_accel = 'Section "InputClass"\n'
    no_accel += '    Identifier "My Mouse"\n'
    no_accel += '    MatchIsPointer "yes"\n'
    no_accel += '    Option "AccelerationProfile" "-1"\n'
    no_accel += '    Option "AccelerationScheme" "none"\n'
    no_accel += '    Option "AccelSpeed" "-1"\n'
    no_accel += 'EndSection'
    return no_accel

def gen_imwheelrc():
    imwheelrc = '".*"\n'
    imwheelrc += 'None,      Up,   Button4, 3\n'
    imwheelrc += 'None,      Down, Button5, 3\n'
    imwheelrc += 'Control_L, Up,   Control_L|Button4\n'
    imwheelrc += 'Control_L, Down, Control_L|Button5\n'
    imwheelrc += 'Shift_L,   Up,   Shift_L|Button4\n'
    imwheelrc += 'Shift_L,   Down, Shift_L|Button5'
    return imwheelrc

def gen_redshift_conf():
    redshift = '; Global settings for redshift\n'
    redshift += '[redshift]\n'
    redshift += '; Set the day and night screen temperatures\n'
    redshift += 'temp-day=6500\n'
    redshift += 'temp-night=3400'
    return redshift
    
def ask_user():
    user = raw_input("What's your user's name? ")
    return user

def check_if_root():
    # tries to create a folder in /etc/. If it can, then run main.
    try:
        os.mkdir("/etc/foo")
    except OSError:
        print "You need root permission to do this!"
        sys.exit()

def remove_foo():
    # removes test folder
    os.rmdir("/etc/foo")

def finish():
    print "    - Everything worked as expected. "
    remove_foo()

def main():
    user = ask_user()
    users_path = "/home/%s" % (user)
    
    update_and_upgrade()

    no_mouse_accel_content = gen_no_mouse_accel()
    file_path = "/etc/X11/xorg.conf.d/50-mouse-acceleration.conf"
    create_file(file_path, no_mouse_accel_content)
    
    install("imwheel")
    imwheelrc_content = gen_imwheelrc()
    file_path = "%s/.imwheelrc" % (users_path)
    create_file(file_path, imwheelrc_content)
    change_owner(user, file_path)
    
    # This fixes screen tearing on ubuntu
    remove("xserver-xorg-video-intel")

    install("redshift redshift-gtk")
    redshift_conf_content = gen_redshift_conf()
    file_path = "%s/.config/redshift.conf" % (users_path)
    create_file(file_path, redshift_conf_content)
    change_owner(user, file_path)

    install("git virtualbox virtualbox-qt")

    finish()

check_if_root()
main()
